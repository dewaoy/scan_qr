import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Home Page",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Color(0xFFCD853F),
      ),
      body: Column(
        children: [
          Text("Hello World"),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                hintText: "Masukkan Nama",
                 border: OutlineInputBorder(
                   borderSide: BorderSide(
                     width: 1,
                     color:Colors.blueAccent,
                   )
                 )
              ),
            ),
          ),
          ElevatedButton(onPressed: () {}, child: Text("simpan"),
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red
            ),
          ),
        ],
      ),
    );
  }
}
